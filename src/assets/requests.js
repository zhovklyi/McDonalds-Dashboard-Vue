async function getProductsRequest(page=1, perPage=10) {
    const url = '/api/products?' + new URLSearchParams({
        page: page,
        perPage: perPage
    })
    
    const response = await fetch(url, {
        method: 'GET'
    })

    const json = await response.json()
    return json
}

async function editProductRequest(data) {
    const response = await fetch('/api/product', {
        method: 'POST',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function deleteProductRequest(id) {
    const response = await fetch('/api/product?id=' + id, {
        method: 'DELETE'
    })

    const json = await response.json()
    return json
}

async function createProductRequest(data) {
    const response = await fetch('/api/product', {
        method: 'PUT',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function getPositionsRequest(category_id=null, page=1, perPage=5) {
    const params = new URLSearchParams({
        page: page,
        perPage: perPage
    })

    if (category_id) {
        params.append('category_id', category_id)
    }

    const url = '/api/positions?' + params
    
    const response = await fetch(url , {
        method: 'GET'
    })

    const json = await response.json()
    return json
}

async function createPositionRequest(data) {
    const response = await fetch('/api/position', {
        method: 'PUT',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function editPositionRequest(data) {
    const response = await fetch('/api/position', {
        method: 'POST',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function deletePositionRequest(id) {
    const response = await fetch('/api/position?id=' + id, {
        method: 'DELETE'
    })

    const json = await response.json()
    return json
}

async function getCategoriesRequest(page=1, perPage=5) {
    const url = '/api/categories?' + new URLSearchParams({
        page: page,
        perPage: perPage
    })

    const response = await fetch(url, {
        method: 'GET'
    })

    const json = await response.json()
    return json
}

async function createCategoryRequest(data) {
    const response = await fetch('/api/category', {
        method: 'PUT',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function editCategoryRequest(data) {
    const response = await fetch('/api/category', {
        method: 'POST',
        body: JSON.stringify(data)
    })

    const json = await response.json()
    return json
}

async function deleteCategoryRequest(id) {
    const response = await fetch('/api/category?id=' + id, {
        method: 'DELETE'
    })

    const json = await response.json()
    return json
}

export {
    getProductsRequest,
    createProductRequest,
    editProductRequest,
    deleteProductRequest,
    
    getPositionsRequest,
    createPositionRequest,
    deletePositionRequest,
    editPositionRequest,

    getCategoriesRequest,
    createCategoryRequest,
    editCategoryRequest,
    deleteCategoryRequest
}