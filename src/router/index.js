import { createRouter, createWebHistory } from 'vue-router'
import CategoryPage from '../components/pages/CategoryPage.vue'
import ProductPage from '../components/pages/ProductPage.vue'
import PositionsPage from  '../components/pages/PositionsPage.vue'

const routes = [
    {
        path: '/',
        redirect: {
            name: 'categoriesRoute'
        },
        name: 'mainRoute'
    },
    {
        path: '/categories',
        component: CategoryPage ,
        name: 'categoriesRoute'
    },
    {
        path: '/products',
        component: ProductPage,
        name: 'productRoute'
    },
    {
        path: '/positions',
        component: PositionsPage,
        name: 'positionsRoute'
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router